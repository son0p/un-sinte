#pragma once

namespace UnSinte
{

class Ramp
{
public:
    Ramp();
    ~Ramp();

    void setSampleRate (float sr);

    void setTime (float time);
    void setTargetValue (float value);
    void setCurrentValue (float value);

    void process (float* output, int n);

private:
    float sampleRate;
    float bufferSize;

    float time;
    float increment;
    float targetValue;
    float currentValue;

    void calculateIncrement();
};

} // namespace UnSinte

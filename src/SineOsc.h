#pragma once

namespace UnSinte
{

class SineOsc
{
public:
    SineOsc();
    ~SineOsc();

    void setSampleRate (float sr);
    void process (float* inputFreq, float* output, int bufferSize);

private:
    void initTable();

    static const int tableSize = 512;
    static bool tableFull;
    static float table[tableSize + 2];

    float sampleRate = 0.0;
    float index = 0.0;
};

} // namespace UnSinte

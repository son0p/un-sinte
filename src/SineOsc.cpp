#include "SineOsc.h"

#include <cmath>
#include <juce_core/juce_core.h>

namespace UnSinte
{

#define LINEAR 0

SineOsc::SineOsc()
{
    initTable();
}

bool SineOsc::tableFull = false;
float SineOsc::table[tableSize + 2] = { 0.0 };

SineOsc::~SineOsc() {}

void SineOsc::initTable()
{
    if (tableFull)
    {
        return;
    }

    for (int i = 0; i < tableSize + 2; ++i)
    {
        table[i] = std::cos ((float) i * 2 * (float) M_PI / tableSize);
    }
    tableFull = true;
}

void SineOsc::setSampleRate (float sr)
{
    sampleRate = sr;
}

void SineOsc::process (float* freqInput, float* output, int bufferSize)
{
    float freq = freqInput[0]; // TODO frecuencia vectorial.

    // increment
    float incr = freq * tableSize / sampleRate;
    float fracSquare = 0.0;
    float fracCube = 0.0;
    float pos = 0.0;
    float tmp = 0.0;

    // Offset (fase) inicial del oscilador. Por ahora cero.
    // phase = phase < 0 ? 1 + phase : phase;
    const float phase = 0.0;
    int offset = (int) (phase * tableSize) % tableSize;

    // processing loop
    for (int i = 0; i < bufferSize; i++)
    {
        pos = index + (float) offset;

        // clang-format off
        #if !LINEAR
            // cubic interpolation
            float frac = pos - floorf (pos);
            float y0 = (int) pos > 0 ? table[(int) pos - 1] : table[tableSize - 1];
            float y1 = table[(int) pos];
            float y2 = table[(int) pos + 1];
            float y3 = table[(int) pos + 2];
            tmp = y3 + 3.f * y1;
            fracSquare = frac * frac;
            fracCube = frac * fracSquare;
            output[i] = fracCube * (-y0 - 3.f * y2 + tmp) / 6.f
                        + fracSquare * ((y0 + y2) / 2.f - y1)
                        + frac * (y2 + (-2.f * y0 - tmp) / 6.f)
                        + y1;
        #endif
        // clang-format on

        // clang-format off
        #if LINEAR
            // linear interpolation
            float frac = pos - floorf (pos);
            float a = table[(int) pos];
            float b = table[(int) pos + 1];
            output[i] = (a + frac * (b - a));
            index += incr;
        #endif
        // clang-format on

        // Increment index
        index += incr;
        while (index >= tableSize)
        {
            index -= (tableSize);
        }
        while (index < 0)
        {
            index += tableSize;
        }
    }
}

} // namespace UnSinte
